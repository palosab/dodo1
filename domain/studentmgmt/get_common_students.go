package studentmgmt

import (
	"assessment1/model"
	"assessment1/store/studentmgmt"

	"context"
)

func GetCommonStudents(ctx context.Context, store studentmgmt.StudentManagerIface, teacherEmails ...string) (model.GetCommonStudentsResponse, error) {
	if len(teacherEmails) == 0 {
		return model.GetCommonStudentsResponse{
			Students: []string{},
		}, nil
	}
	students, err := store.RetrieveCommonStudents(context.TODO(), teacherEmails...)
	if err != nil {
		return model.GetCommonStudentsResponse{
			Students: []string{},
		}, err
	}

	var studentEmails = []string{}
	for _, student := range students {
		studentEmails = append(studentEmails, student.Email)

	}
	return model.GetCommonStudentsResponse{
		Students: studentEmails,
	}, nil
}
