package studentmgmt

import (
	"strings"
)

/*
	GetMentioned gets list of @mentioned emails. Does not validate email

Assumes that every @ tag must be preceded by a space character.
*/
func GetMentioned(text string) []string {
	output := strings.Split(text, " @")
	if len(output) > 0 {
		output = output[1:]
	}
	return output
}
