package studentmgmt

import (
	"assessment1/model"
	"assessment1/store/studentmgmt"
	"context"
)

func RetrieveStudentsForNotification(ctx context.Context, store studentmgmt.StudentManagerIface, request model.RetrieveForNotificationRequest) (model.RetrieveForNotificationResponse, error) {
	var (
		response model.RetrieveForNotificationResponse
	)
	mentions := GetMentioned(request.Notification)
	students, err := store.RetrieveStudentsForNotification(ctx, request.TeacherEmail, mentions...)
	if err != nil {
		return response, err
	}

	for _, student := range students {
		response.Recipients = append(response.Recipients, student.Email)
	}

	return response, nil

}
