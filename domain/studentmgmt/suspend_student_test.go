package studentmgmt_test

import (
	"assessment1/mocks"
	"context"
	"database/sql"
	"errors"
	"testing"

	. "assessment1/domain/studentmgmt"

	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func Test_SuspendStudent_ErrInvalidStudent(t *testing.T) {
	mockedStore := &mocks.StudentManagerIface{}
	mockedStore.On("SuspendStudentByEmail", mock.Anything, mock.Anything, mock.Anything).Return(nil, sql.ErrNoRows).Once()

	err := SuspendStudent(context.TODO(), mockedStore, "student@email.com")
	assert.Error(t, err)
	assert.Equal(t, "student does not exist", err.Error())

}
func Test_SuspendStudent_ErrServerError(t *testing.T) {
	mockedStore := &mocks.StudentManagerIface{}
	mockedStore.On("SuspendStudentByEmail", mock.Anything, mock.Anything, mock.Anything).Return(nil, pq.ErrSSLNotSupported).Once()

	err := SuspendStudent(context.TODO(), mockedStore, "student@email.com")
	assert.Error(t, err)
	assert.Equal(t, "pq: SSL is not enabled on the server", err.Error())

}
func Test_SuspendStudent_ErrSomeRandomError(t *testing.T) {
	mockedStore := &mocks.StudentManagerIface{}
	mockedStore.On("SuspendStudentByEmail", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("random")).Once()

	err := SuspendStudent(context.TODO(), mockedStore, "student@email.com")
	assert.Error(t, err)
	assert.Equal(t, "random", err.Error())

}
func Test_SuspendStudent_Success(t *testing.T) {
	mockedStore := &mocks.StudentManagerIface{}
	mockedStore.On("SuspendStudentByEmail", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil).Once()

	err := SuspendStudent(context.TODO(), mockedStore, "student@email.com")
	assert.NoError(t, err)

}
