package studentmgmt_test

import (
	. "assessment1/domain/studentmgmt"
	"assessment1/mocks"
	"assessment1/model"
	"assessment1/store"
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestRetrieveStudentsForNotification_HapyFlow(t *testing.T) {
	mockedStore := &mocks.StudentManagerIface{}
	mockedStore.On("RetrieveStudentsForNotification",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(
		[]store.Student{
			{Email: "email1@email.com"},
			{Email: "email2@email.com"},
		}, nil).Once()
	resp, err := RetrieveStudentsForNotification(context.TODO(), mockedStore, model.RetrieveForNotificationRequest{
		TeacherEmail: "teacher@email.com",
	})

	assert.NoError(t, err)
	assert.NotEmpty(t, resp.Recipients)

}
