package studentmgmt

import (
	"assessment1/model"
	"assessment1/store/studentmgmt"
	"context"
	"database/sql"
	"net/http"
)

/*
RegisterStudents returns a list of students that failed to be registered.

returns an error if the registration is partially successful
*/
func RegisterStudents(ctx context.Context, store studentmgmt.StudentManagerIface, input model.RegisterStudentRequest) error {
	teacher, err := store.FindTeacherByEmail(ctx, input.Teacher)

	if err != nil {
		if err == sql.ErrNoRows {
			return model.NewError(model.ErrEntityNotFound, "teacher does not exist", http.StatusNotFound, nil)
		}
		return err
	}

	registered, invalidUsers, err := store.RegisterStudents(nil, *teacher.ID, input.Students...)
	if err != nil {
		return err
	}
	if len(invalidUsers) != 0 {
		return model.NewError("api.register_students.invalid_students",
			"register partially successful",
			http.StatusAccepted, model.RegisterStudentResponse{
				RegisteredStudent: registered,
				InvalidStudent:    invalidUsers,
			})
	}

	return nil
}
