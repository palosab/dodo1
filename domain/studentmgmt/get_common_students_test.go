package studentmgmt_test

import (
	. "assessment1/domain/studentmgmt"
	"assessment1/mocks"
	"assessment1/store"
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestGetCommonStudents_NoStudents(t *testing.T) {
	mockedStore := &mocks.StudentManagerIface{}
	mockedStore.On("RetrieveCommonStudents", mock.Anything, mock.Anything).Return(
		[]store.Student{},
		nil,
	).Once()
	teachers := []string{"teacher1@gmail.com"}
	response, err := GetCommonStudents(context.TODO(), mockedStore, teachers...)
	assert.NoError(t, err)
	assert.Empty(t, response.Students)

}
func TestGetCommonStudents_OneStudent(t *testing.T) {
	mockedStore := &mocks.StudentManagerIface{}
	mockedStore.On("RetrieveCommonStudents", mock.Anything, mock.Anything).Return(
		[]store.Student{
			{Email: "hello@gmail.com"},
			{Email: "bell@gmail.com"},
		},
		nil,
	).Once()
	teachers := []string{"teacher1@gmail.com"}
	response, err := GetCommonStudents(context.TODO(), mockedStore, teachers...)
	assert.NoError(t, err)
	assert.NotEmpty(t, response.Students)
	assert.Equal(t, []string{"hello@gmail.com", "bell@gmail.com"}, response.Students)

}
func TestGetCommonStudents_NoTeacher(t *testing.T) {
	mockedStore := &mocks.StudentManagerIface{}
	mockedStore.On("RetrieveCommonStudents", mock.Anything, mock.Anything).Return(
		[]store.Student{},
		nil,
	).Once()
	teachers := []string{}
	list, err := GetCommonStudents(context.TODO(), mockedStore, teachers...)
	assert.NoError(t, err)
	assert.Empty(t, list.Students)

}
