package studentmgmt_test

import (
	. "assessment1/domain/studentmgmt"
	"assessment1/mocks"
	"assessment1/model"
	"assessment1/store"
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestRegisterStudentsHappyFlow(t *testing.T) {
	mockedStore := &mocks.StudentManagerIface{}
	request := model.RegisterStudentRequest{
		Teacher: "hello123",
		Students: []string{
			"student1@gmail.com",
		},
	}

	var teachID string = "123"
	returnedTeacher := &store.Teacher{
		ID:    &teachID,
		Email: "ex@gmal.com",
	}
	mockedStore.On("FindTeacherByEmail",
		mock.Anything,
		mock.Anything).Return(returnedTeacher, nil).Once()
	mockedStore.On("RegisterStudents",
		mock.Anything,
		mock.Anything,
		mock.Anything,
	).Return([]string{"student1@gmail.com"}, []string{}, nil).Once()

	err := RegisterStudents(context.TODO(), mockedStore, request)
	assert.NoError(t, err)

}
func TestRegisterStudentsPartialSuccess(t *testing.T) {
	mockedStore := &mocks.StudentManagerIface{}
	request := model.RegisterStudentRequest{
		Teacher: "hello123",
		Students: []string{
			"student1@gmail.com",
			"student2@gmail.com",
		},
	}
	var teachID string = "123"
	returnedTeacher := &store.Teacher{
		ID:    &teachID,
		Email: "ex@gmal.com",
	}
	mockedStore.On("FindTeacherByEmail",
		mock.Anything,
		mock.Anything).Return(returnedTeacher, nil).Once()
	mockedStore.On("RegisterStudents",
		mock.Anything,
		mock.Anything,
		mock.Anything,
	).Return([]string{"student1@gmail.com"}, []string{"student2@gmail.com"}, nil).Once()

	err := RegisterStudents(context.TODO(), mockedStore, request)
	assert.Equal(t, "register partially successful", err.Error())

}
