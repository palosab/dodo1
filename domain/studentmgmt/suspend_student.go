package studentmgmt

import (
	"assessment1/model"
	"assessment1/store/studentmgmt"
	"context"
	"database/sql"
	"net/http"
)

func SuspendStudent(ctx context.Context, store studentmgmt.StudentManagerIface, studentEmail string) error {
	if _, err := store.SuspendStudentByEmail(ctx, studentEmail); err != nil {
		if err == sql.ErrNoRows {
			return model.NewError(model.ErrEntityNotFound, "student does not exist", http.StatusNotFound, nil)
		}
		return err
	}
	return nil

}
