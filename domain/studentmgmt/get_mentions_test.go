package studentmgmt_test

import (
	"testing"

	. "assessment1/domain/studentmgmt"

	"github.com/stretchr/testify/assert"
)

func TestGetMentioned(t *testing.T) {
	asserter := assert.New(t)
	input := "Hello students! @studentagnes@gmail.com @studentmiche@gmail.com"
	results := GetMentioned(input)
	asserter.NotNil(results)
	asserter.Equal([]string{"studentagnes@gmail.com", "studentmiche@gmail.com"}, results)
}
