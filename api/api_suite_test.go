package api_test

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/stretchr/testify/assert"
)

func TestApi(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Api Suite")
}

func marshalJson(data interface{}, asserter *assert.Assertions) []byte {
	jsn, err := json.Marshal(data)
	asserter.NoError(err)
	return jsn
}

func unmarshalJson(ptr interface{}, r *http.Response, asserter *assert.Assertions) {
	data, err := ioutil.ReadAll(r.Body)
	asserter.NoError(err)
	asserter.NoError(json.Unmarshal(data, ptr))
}
