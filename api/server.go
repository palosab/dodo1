package api

import (
	studentmgmt "assessment1/store/studentmgmt"
	"net/http"

	"github.com/gorilla/mux"
)

type Server struct {
	store studentmgmt.StudentManagerIface
}

// NewServer returns a new server
func NewServer(datastore studentmgmt.StudentManagerIface) *Server {
	return &Server{
		store: datastore,
	}
}

// routes for student management
const (
	RegisterStudentsAPI                = "/register"
	GetCommonStudentsAPI               = "/commonstudents"
	SuspendStudentAPI                  = "/suspend"
	RetrieveStudentsForNotificationAPI = "/retrievefornotifications"
)

func (s *Server) SetupRoute(r *mux.Router) {
	r.Methods(http.MethodPost).Path(RegisterStudentsAPI).HandlerFunc(s.RegisterStudents)
	r.Methods(http.MethodGet).Path(GetCommonStudentsAPI).HandlerFunc(s.GetCommonStudents)
	r.Methods(http.MethodPost).Path(SuspendStudentAPI).HandlerFunc(s.SuspendStudent)
	r.Methods(http.MethodPost).Path(RetrieveStudentsForNotificationAPI).HandlerFunc(s.RetrieveStudentsForNotification)

}
