package api_test

import (
	"assessment1/api"
	"assessment1/mocks"
	"assessment1/model"
	"assessment1/store"
	"bytes"
	"database/sql"
	"io/ioutil"
	"net/http"
	"net/http/httptest"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var _ = Describe("Student Mgmt", func() {
	storeMock := &mocks.StudentManagerIface{}
	var (
		w        *httptest.ResponseRecorder
		asserter *assert.Assertions
	)
	BeforeEach(func() {
		w = httptest.NewRecorder()
		asserter = assert.New(GinkgoT())
	})

	Describe("Test RegisterStudents", func() {
		Context("valid students are provided", func() {
			It("all students successfully registered, returns http no content", func() {
				request := model.RegisterStudentRequest{
					Teacher: "helloteacher@gmail.com",
					Students: []string{
						"student1@gmail.com",
						"student2@gmail.com",
					},
				}
				var teachID string = "123"
				returnedTeacher := &store.Teacher{
					ID:    &teachID,
					Email: "ex@gmal.com",
				}
				storeMock.On("FindTeacherByEmail",
					mock.Anything,
					mock.Anything).Return(returnedTeacher, nil).Once()
				storeMock.On("RegisterStudents",
					mock.Anything,
					mock.Anything,
					mock.Anything,
				).Return([]string{"student1@gmail.com", "student2@gmail.com"}, []string{}, nil).Once()
				data := marshalJson(request, asserter)
				s := api.NewServer(storeMock)
				req := httptest.NewRequest(http.MethodPost, "/api"+api.RegisterStudentsAPI,
					bytes.NewBuffer(data))
				s.RegisterStudents(w, req)
				res := w.Result()
				defer res.Body.Close()
				data, err := ioutil.ReadAll(res.Body)
				Expect(err).To(BeNil())
				Expect(res.StatusCode).To(BeEquivalentTo(http.StatusNoContent))
			})
		})
		Context("a mixture of valid/invalid students provided", func() {
			It("partially registered, returns status 202", func() {
				request := model.RegisterStudentRequest{
					Teacher: "helloteacher@gmail.com",
					Students: []string{
						"student1@gmail.com",
						"student2@gmail.com",
					},
				}
				var teachID string = "123"
				returnedTeacher := &store.Teacher{
					ID:    &teachID,
					Email: "ex@gmal.com",
				}
				storeMock.On("FindTeacherByEmail",
					mock.Anything,
					mock.Anything).Return(returnedTeacher, nil).Once()
				storeMock.On("RegisterStudents",
					mock.Anything,
					mock.Anything,
					mock.Anything,
				).Return([]string{"student1@gmail.com"}, []string{"student2@gmail.com"}, nil).Once()
				data := marshalJson(request, asserter)
				s := api.NewServer(storeMock)
				req := httptest.NewRequest(http.MethodPost, "/api"+api.RegisterStudentsAPI,
					bytes.NewBuffer(data))
				s.RegisterStudents(w, req)
				res := w.Result()
				defer res.Body.Close()
				data, err := ioutil.ReadAll(res.Body)
				Expect(err).To(BeNil())
				Expect(res.StatusCode).To(BeEquivalentTo(http.StatusAccepted))
			})
		})
		Context("invalid reuqest payoad provided", func() {
			It("empty struct - should return http Bad request", func() {
				s := api.NewServer(storeMock)
				var jsonstr = []byte(`{}`)
				req := httptest.NewRequest(http.MethodPost, "/api"+api.RegisterStudentsAPI,
					bytes.NewBuffer(jsonstr))
				s.RegisterStudents(w, req)

				res := w.Result()
				defer res.Body.Close()
				_, err := ioutil.ReadAll(res.Body)
				Expect(err).To(BeNil())
				Expect(res.StatusCode).To(BeEquivalentTo(http.StatusBadRequest))

			})
		})

	})

	Describe("Test GetCommonStudents", func() {
		Context("no teachers provided in query url", func() {
			It("empty list of student should be returned", func() {
				url := "/api" + api.GetCommonStudentsAPI
				s := api.NewServer(storeMock)
				req := httptest.NewRequest(http.MethodGet, url, nil)
				s.GetCommonStudents(w, req)
				res := w.Result()
				defer res.Body.Close()
				result := model.GetCommonStudentsResponse{}
				unmarshalJson(&result, res, asserter)
				Expect(result.Students).To(BeEmpty())

				// Expect(res.StatusCode).To(BeEquivalentTo(http.StatusBadRequest))
			})
		})
	})

	Describe("Test SuspendStudent", func() {
		Context("student email does not exist in system", func() {
			It("returns bad request", func() {
				storeMock.On("SuspendStudentByEmail",
					mock.Anything,
					mock.Anything,
				).Return(nil, sql.ErrNoRows).Once()

				s := api.NewServer(storeMock)
				var jsonstr = []byte(`{"student":"abc@gmail.com"}`)
				req := httptest.NewRequest(http.MethodPost, "/api"+api.RegisterStudentsAPI,
					bytes.NewBuffer(jsonstr))
				s.SuspendStudent(w, req)

				res := w.Result()
				defer res.Body.Close()
				_, err := ioutil.ReadAll(res.Body)
				Expect(err).To(BeNil())
				Expect(res.StatusCode).To(BeEquivalentTo(http.StatusNotFound))
			})
		})
		Context("student email format invalid", func() {
			It("", func() {
				s := api.NewServer(storeMock)
				var jsonstr = []byte(`{"student":"abc.com"}`)
				req := httptest.NewRequest(http.MethodPost, "/api"+api.RegisterStudentsAPI,
					bytes.NewBuffer(jsonstr))
				s.SuspendStudent(w, req)

				res := w.Result()
				defer res.Body.Close()
				_, err := ioutil.ReadAll(res.Body)
				Expect(err).To(BeNil())
				Expect(res.StatusCode).To(BeEquivalentTo(http.StatusBadRequest))
			})
		})
		Context("Happy flow", func() {
			It("success, returns no content", func() {
				storeMock.On("SuspendStudentByEmail",
					mock.Anything,
					mock.Anything,
				).Return(&store.Student{}, nil).Once()
				s := api.NewServer(storeMock)
				var jsonstr = []byte(`{"student":"abc@gmail.com"}`)
				req := httptest.NewRequest(http.MethodPost, "/api"+api.SuspendStudentAPI,
					bytes.NewBuffer(jsonstr))
				s.SuspendStudent(w, req)

				res := w.Result()
				defer res.Body.Close()
				_, err := ioutil.ReadAll(res.Body)
				Expect(err).To(BeNil())
				Expect(res.StatusCode).To(BeEquivalentTo(http.StatusNoContent))
			})
		})
	})

	Describe("Test RetrieveStudentsForNotification", func() {
		Context("Happy Flow", func() {
			It("returns a list of intersecting students", func() {
				storeMock.On("RetrieveStudentsForNotification",
					mock.Anything,
					mock.Anything,
					mock.Anything,
				).Return([]store.Student{
					{Email: "hello@baby.com"},
				}, nil).Once()
				s := api.NewServer(storeMock)

				request := model.RetrieveForNotificationRequest{
					TeacherEmail: "teacher@email.com",
				}
				data := marshalJson(request, asserter)

				req := httptest.NewRequest(http.MethodPost, "/api"+api.RetrieveStudentsForNotificationAPI,
					bytes.NewBuffer(data))

				s.RetrieveStudentsForNotification(w, req)

				res := w.Result()
				defer res.Body.Close()
				Expect(res.StatusCode).To(BeEquivalentTo(http.StatusOK))

				resData := model.RetrieveForNotificationResponse{}
				unmarshalJson(&resData, res, asserter)
				asserter.NotEmpty(resData.Recipients)
				asserter.Equal(1, len(resData.Recipients))

			})
		})
		Context("Bad payload", func() {
			It("returns http bad request", func() {
				s := api.NewServer(storeMock)

				request := model.RetrieveForNotificationRequest{
					TeacherEmail: "teacheremail.com",
				}
				data := marshalJson(request, asserter)

				req := httptest.NewRequest(http.MethodPost, "/api"+api.RetrieveStudentsForNotificationAPI,
					bytes.NewBuffer(data))

				s.RetrieveStudentsForNotification(w, req)

				res := w.Result()
				defer res.Body.Close()
				Expect(res.StatusCode).To(BeEquivalentTo(http.StatusBadRequest))

			})

		})
	})
})
