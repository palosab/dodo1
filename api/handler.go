package api

import (
	"assessment1/model"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/lib/pq"
)

var val = validator.New()

func (s *Server) unmarshalJSON(r *http.Request, input interface{}) error {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return model.NewError(model.ErrBadRequest, err.Error(), http.StatusBadRequest, nil)
	}
	if err := json.Unmarshal(b, input); err != nil {
		return model.NewError(model.ErrBadRequest, err.Error(), http.StatusBadRequest, nil)
	}

	if err := val.Struct(input); err != nil {
		return model.NewError(model.ErrBadRequest, err.Error(), http.StatusBadRequest, nil)
	}
	return nil
}

func (s *Server) handleError(w http.ResponseWriter, err error) {
	w.Header().Set("Content-Type", "application/json")

	var badInputErr *model.ErrorResponse
	if errors.As(err, &badInputErr) {
		w.WriteHeader(badInputErr.Status)
		json.NewEncoder(w).Encode(badInputErr)
		return
	}

	var pqError *pq.Error
	if errors.As(err, &pqError) {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(model.NewError(model.ErrInternalServerErrorDB, err.Error(), http.StatusInternalServerError, nil))
		return
	}

	w.WriteHeader(http.StatusInternalServerError)
	json.NewEncoder(w).Encode(model.ErrorResponse{
		Message: err.Error(),
	})
}

func (s *Server) setResponse(w http.ResponseWriter, status int, payload interface{}) {

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(payload)

}
