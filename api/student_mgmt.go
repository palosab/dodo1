package api

import (
	"assessment1/domain/studentmgmt"
	"assessment1/model"
	"context"
	"net/http"
)

// RegisterStudents handler.
func (me *Server) RegisterStudents(w http.ResponseWriter, r *http.Request) {
	var input = model.RegisterStudentRequest{}
	if err := me.unmarshalJSON(r, &input); err != nil {
		me.handleError(w, err)
		return
	}

	err := studentmgmt.RegisterStudents(context.TODO(), me.store, input)
	if err != nil {
		me.handleError(w, err)
		return
	}

	me.setResponse(w, http.StatusNoContent, nil)

}

func (me *Server) GetCommonStudents(w http.ResponseWriter, r *http.Request) {
	var (
		urlParams     = r.URL.Query()
		teacherEmails = urlParams["teacher"]
	)
	response, err := studentmgmt.GetCommonStudents(context.TODO(), me.store, teacherEmails...)
	if err != nil {
		me.handleError(w, err)
		return
	}
	me.setResponse(w, http.StatusOK, response)

}

func (me *Server) SuspendStudent(w http.ResponseWriter, r *http.Request) {
	input := model.SuspendStudentRequest{}
	if err := me.unmarshalJSON(r, &input); err != nil {
		me.handleError(w, err)
		return
	}
	if err := studentmgmt.SuspendStudent(context.TODO(), me.store, input.StudentEmail); err != nil {
		me.handleError(w, err)
		return
	}
	me.setResponse(w, http.StatusNoContent, nil)
}

func (me *Server) RetrieveStudentsForNotification(w http.ResponseWriter, r *http.Request) {
	input := model.RetrieveForNotificationRequest{}
	if err := me.unmarshalJSON(r, &input); err != nil {
		me.handleError(w, err)
		return
	}
	response, err := studentmgmt.RetrieveStudentsForNotification(context.TODO(), me.store, input)
	if err != nil {
		me.handleError(w, err)
		return
	}
	me.setResponse(w, http.StatusOK, response)

}
