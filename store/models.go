package store

import "time"

// StudentRegister datastruct for `student_register`
type StudentRegister struct {
	StudentID    *string    `db:"student_id"`
	TeacherID    *string    `db:"teacher_id"`
	RegisteredAt *time.Time `db:"registered_at"`
	UpdatedAt    *time.Time `db:"updated_at"`
	CreatedAt    *time.Time `db:"created_at"`
}

// Student datastruct for `student` table
type Student struct {
	ID        *string    `db:"id"`
	Email     string     `db:"email"`
	Suspended bool       `db:"suspended"`
	CreatedAt *time.Time `db:"created_at"`
	UpdatedAt *time.Time `db:"updated_at"`
}

// Student datastruct for `student` table
type Teacher struct {
	ID        *string    `db:"id"`
	Email     string     `db:"email"`
	CreatedAt *time.Time `db:"created_at"`
	UpdatedAt *time.Time `db:"updated_at"`
}
