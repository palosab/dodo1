CREATE TABLE IF NOT EXISTS student_register(
    student_id uuid,
    teacher_id uuid,
    created_at timestamp without time zone default current_timestamp,
    updated_at timestamp without time zone default null,
    UNIQUE (student_id, teacher_id)  
);


