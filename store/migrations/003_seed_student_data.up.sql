insert into student (id, email) values 
(uuid_generate_v4(), 'studentjon@gmail.com'),
(uuid_generate_v4(), 'studenthon@gmail.com'),
(uuid_generate_v4(), 'commonstudent1@gmail.com'),
(uuid_generate_v4(), 'commonstudent2@gmail.com'),
(uuid_generate_v4(), 'student_only_under_teacher_ken@gmail.com')
ON CONFLICT DO NOTHING;


