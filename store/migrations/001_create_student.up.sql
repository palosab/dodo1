CREATE TABLE IF NOT EXISTS student(
    id uuid,
    email varchar,
    suspended bool default false,
    created_at timestamp without time zone default current_timestamp,
    updated_at timestamp without time zone default null,
    PRIMARY KEY(id),
    UNIQUE (email)  
);


