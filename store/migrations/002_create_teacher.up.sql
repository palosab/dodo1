CREATE TABLE IF NOT EXISTS teacher(
    id uuid,
    email varchar,
    created_at timestamp without time zone default current_timestamp,
    updated_at timestamp without time zone default null,
    PRIMARY KEY(id),
    UNIQUE (email)  
);


