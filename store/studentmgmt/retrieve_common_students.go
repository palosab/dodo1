package studentmgmt

import (
	"assessment1/store"
	"context"

	"github.com/jmoiron/sqlx"
)

// RetrieveCommonStudents retrieve list of students using teacher email
func (me *PgStore) RetrieveCommonStudents(ctx context.Context, teacherEmails ...string) ([]store.Student, error) {
	var (
		students []store.Student
	)
	query, args, err := sqlx.In(`WITH cdm AS (
		SELECT s.email s_email, t.email t_email FROM student_register sr
		JOIN student s on s.id=sr.student_id
		JOIN teacher t on t.id=sr.teacher_id
		WHERE t.email in (?)
		) SELECT s_email AS email FROM cdm 
		GROUP BY 1
		HAVING COUNT(DISTINCT t_email)=?
		`, teacherEmails, len(teacherEmails))

	if err != nil {
		return nil, err
	}
	query = me.DB.Rebind(query)
	if err = me.DB.Select(&students, query, args...); err != nil {
		return nil, err
	}
	return students, nil
}
