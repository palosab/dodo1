package studentmgmt_test

import (
	"testing"

	. "assessment1/store/studentmgmt"

	"database/sql"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestSuspendStudentByEmail_Success(t *testing.T) {
	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")
	rows := sqlmock.NewRows([]string{"id", "email"}).
		AddRow("123456", "email2@gmail.com")

	mock.ExpectQuery(`UPDATE student SET suspended=true`).
		WithArgs("taggedStudent@gmail.com").
		WillReturnRows(rows)

	ds := NewStore(sqlxDB)
	student, err := ds.SuspendStudentByEmail(nil, "taggedStudent@gmail.com")
	assert.NoError(t, err)
	assert.NotNil(t, student)
	assert.NoError(t, mock.ExpectationsWereMet())
}
func TestSuspendStudentByEmail_OnInexistantStudent(t *testing.T) {
	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")
	rows := sqlmock.NewRows([]string{"id", "email"})

	mock.ExpectQuery(`UPDATE student SET`).
		WithArgs("taggedStudent@gmail.com").
		WillReturnRows(rows)

	ds := NewStore(sqlxDB)
	_, err := ds.SuspendStudentByEmail(nil, "taggedStudent@gmail.com")
	assert.Equal(t, sql.ErrNoRows, err)
}
