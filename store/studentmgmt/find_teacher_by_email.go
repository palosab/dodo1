package studentmgmt

import (
	"assessment1/store"
	"context"
)

func (me *PgStore) FindTeacherByEmail(ctx context.Context, teacherEmail string) (*store.Teacher, error) {
	var teacher store.Teacher
	err := me.DB.Get(&teacher, "SELECT id, email FROM teacher where email=$1", teacherEmail)
	if err != nil {
		return nil, err
	}
	return &teacher, nil
}
