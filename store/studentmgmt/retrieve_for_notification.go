package studentmgmt

import (
	"assessment1/store"
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

func (me *PgStore) RetrieveStudentsForNotification(ctx context.Context, teacherEmail string, taggedStudentEmails ...string) ([]store.Student, error) {
	var (
		students []store.Student
		arg      = map[string]interface{}{
			"teacher_email": teacherEmail,
		}
		queryStr = `SELECT s.email from student_register sr
		RIGHT JOIN student s on s.id=sr.student_id
		LEFT JOIN teacher t on t.id=sr.teacher_id
		WHERE (
			(t.email=:teacher_email)
			%s
		)
		AND s.suspended=false
		GROUP BY 1`
		tagCondition = ""
	)

	if len(taggedStudentEmails) != 0 {
		tagCondition = `OR
		(s.email IN(:tagged_students))`
		arg["tagged_students"] = taggedStudentEmails
	}

	query := fmt.Sprintf(queryStr, tagCondition)
	query, args, err := sqlx.Named(query, arg)
	if err != nil {
		return nil, err
	}
	if len(taggedStudentEmails) != 0 {
		query, args, err = sqlx.In(query, args...)
		if err != nil {
			return nil, err
		}
	}

	query = me.DB.Rebind(query)
	if err := me.DB.Select(&students, query, args...); err != nil {
		return students, err
	}
	return students, err

}
