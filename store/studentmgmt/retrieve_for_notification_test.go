package studentmgmt_test

import (
	"testing"

	. "assessment1/store/studentmgmt"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestRetrieveForNotification_WithMessage(t *testing.T) {
	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")
	rows := sqlmock.NewRows([]string{"email"}).
		AddRow("email1@gmail.com").
		AddRow("email2@gmail.com").
		AddRow("taggedStudent@gmail.com")

	mock.ExpectQuery(`SELECT s.email from student_register sr (.+)`).
		WithArgs("teacher@gmail.com", "taggedStudent@gmail.com").
		WillReturnRows(rows)

	ds := NewStore(sqlxDB)
	list, err := ds.RetrieveStudentsForNotification(nil, "teacher@gmail.com", "taggedStudent@gmail.com")
	assert.NoError(t, err)
	assert.NotEmpty(t, list)
	assert.Equal(t, 3, len(list))
	assert.NoError(t, mock.ExpectationsWereMet())
}
