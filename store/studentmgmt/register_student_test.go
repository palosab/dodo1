package studentmgmt_test

import (
	"testing"
	"time"

	. "assessment1/store/studentmgmt"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestRegisterStudents_OneStudent(t *testing.T) {
	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")
	mock.ExpectBegin()
	row := sqlmock.NewRows([]string{"teacher_id", "student_id", "registered_at", "updated_at"}).AddRow("teacher", "student", time.Now(), time.Now())
	mock.ExpectQuery(`INSERT INTO student_register (.+)`).
		WithArgs("teacher", "student").
		WillReturnRows(row)

	ds := NewStore(sqlxDB)
	registered, unregistered, err := ds.RegisterStudents(nil, "teacher", "student")
	assert.NoError(t, err)
	assert.Empty(t, unregistered)
	assert.NotEmpty(t, registered)
	assert.Equal(t, registered[0], "student")
	assert.NoError(t, mock.ExpectationsWereMet())
}
