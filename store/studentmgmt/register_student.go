package studentmgmt

import (
	"assessment1/store"
	"context"

	"github.com/jmoiron/sqlx"
)

/*
RegisterStudents initiates db trx transaction and iteratively registers students to instructors.

commits at the end if all is well
*/
func (me *PgStore) RegisterStudents(ctx context.Context, teacherID string, studentEmails ...string) ([]string, []string, error) {
	var (
		loopErr      error
		registered   = []string{}
		invalidUsers = []string{}
	)

	tx, err := me.DB.Beginx()
	if err != nil {
		return registered, invalidUsers, err
	}

	defer func() {
		if loopErr != nil {
			tx.Rollback()
		} else {
			if err := tx.Commit(); err != nil {
				loopErr = err
			}
		}
	}()
	for _, studentEmail := range studentEmails {
		var added bool
		added, loopErr = registerStudent(tx, teacherID, studentEmail)
		if loopErr != nil {
			break
		}
		if added {
			registered = append(registered, studentEmail)
		} else {
			invalidUsers = append(invalidUsers, studentEmail)
		}
	}

	return registered, invalidUsers, loopErr
}

/*
registerStudent registers a student to teacher in a tx session.
returns false if student does not exist
*/
func registerStudent(tx *sqlx.Tx, teacherID, studentEmail string) (bool, error) {
	sql := `INSERT INTO student_register (teacher_id, student_id)
	VALUES (
	  $1,
	  (select id from student s where s.email =$2)
	) ON CONFLICT(teacher_id, student_id) DO UPDATE SET updated_at=now() returning *;
	`
	var register store.StudentRegister
	row := tx.QueryRowx(sql, teacherID, studentEmail)
	err := row.StructScan(&register)
	if err != nil {
		return false, err
	}
	// returns false if student_id=null
	//student_id null -> student doesnt exist
	if register.StudentID == nil {
		return false, nil
	}

	return true, nil
}
