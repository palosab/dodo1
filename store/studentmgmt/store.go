package studentmgmt

import (
	"assessment1/store"
	"context"

	"github.com/jmoiron/sqlx"
)

type PgStore struct {
	DB *sqlx.DB
}

func NewStore(db *sqlx.DB) *PgStore {
	return &PgStore{
		DB: db,
	}
}

// StudentManagerIface represents actions for student management
type StudentManagerIface interface {
	FindTeacherByEmail(ctx context.Context, teacherEmail string) (*store.Teacher, error)
	RegisterStudents(ctx context.Context, teacherEmail string, studentEmails ...string) ([]string, []string, error)
	RetrieveCommonStudents(ctx context.Context, teacherEmails ...string) ([]store.Student, error)
	SuspendStudentByEmail(ctx context.Context, studentEmail string) (*store.Student, error)
	RetrieveStudentsForNotification(ctx context.Context, teacherEmail string, studentEmails ...string) ([]store.Student, error)
}

var _ StudentManagerIface = (*PgStore)(nil)
