package studentmgmt

import (
	"assessment1/store"
	"context"
)

// SuspendStudent suspends a user and returns resulting student object
func (me *PgStore) SuspendStudentByEmail(ctx context.Context, studentEmail string) (*store.Student, error) {
	sqlQuery := `UPDATE student SET suspended=true, updated_at=now() WHERE email=$1 returning id, email;`
	row := me.DB.QueryRowx(sqlQuery, studentEmail)
	var student store.Student
	if err := row.StructScan(&student); err != nil {
		return nil, err
	}
	return &student, nil

}
