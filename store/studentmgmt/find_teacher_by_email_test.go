package studentmgmt_test

import (
	"context"
	"database/sql"
	"testing"

	. "assessment1/store/studentmgmt"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestFindTeacherByEmail_HappyFlow(t *testing.T) {

	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")
	row := sqlmock.NewRows([]string{"id", "email"}).AddRow("123", "e@mail,com")
	mock.ExpectQuery(`SELECT id, email FROM teacher where email(.+)`).
		WithArgs("teacher@email.com").
		WillReturnRows(row)

	ds := NewStore(sqlxDB)
	teacher, err := ds.FindTeacherByEmail(context.TODO(), "teacher@email.com")

	assert.NoError(t, err)
	assert.NotNil(t, teacher)
	assert.NoError(t, mock.ExpectationsWereMet())

}
func TestFindTeacherByEmail_NoResults(t *testing.T) {

	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")
	row := sqlmock.NewRows([]string{"id", "email"})
	mock.ExpectQuery(`SELECT id, email FROM teacher where email(.+)`).
		WithArgs("teacher@email.com").
		WillReturnRows(row)

	ds := NewStore(sqlxDB)
	_, err := ds.FindTeacherByEmail(context.TODO(), "teacher@email.com")

	assert.Equal(t, sql.ErrNoRows, err)
	assert.NoError(t, mock.ExpectationsWereMet())

}
