package studentmgmt_test

import (
	. "assessment1/store/studentmgmt"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestRetrieveCommonStudents_HappyFlow(t *testing.T) {
	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")
	rows := sqlmock.NewRows([]string{"email"}).AddRow("TestUser1").AddRow("TestUser2")
	mock.ExpectQuery(`WITH cdm AS (.+)
	SELECT s.email s_email, t.email t_email FROM student_register sr (.+)`).
		WithArgs("a", "b", 2).
		WillReturnRows(rows)

	ds := NewStore(sqlxDB)
	list, err := ds.RetrieveCommonStudents(nil, "a", "b")
	assert.NoError(t, err)
	assert.Equal(t, 2, len(list))
	assert.NoError(t, mock.ExpectationsWereMet())
}
