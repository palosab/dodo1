
# Prerequisites #
1. golang 1.19
2. [mockery](https://github.com/vektra/mockery) for generating mocks for Golang interfaces
3. docker & docker compose

# Introduction #
This repo consists of:
- the service, written in golang
- the sql migration scripts, detailed in the following section
- dockerfile
- docker-compose



## Assumptions ##
- login and access control have already been handled.
- no triggers were set on the database tables. see the `Notes` section

## schema migration ##
database: `postgresql:14.5`

within this project, for the sake of convenience,schema migration is run before the application server starts. the schema files are stored in `store/migrations`, and migrated using [golang-migrate](https://github.com/golang-migrate/migrate)

the system has also been seeded with some test data (see files [004](store/migrations/004_seed_teacher_data.up.sql), [005](store/migrations/005_create_student_register.up.sql), [006](006_seed_student_registration.up.sql) )for the sake of demonstration.

## makefile ##
a makefile is provided to improve QOL! :D

## Running the service ##
Running 
```
make dockerup
```

will launch a multi-comtainer application containing the postgres db instance and the application instance, with the environment variables as defined at the top of the Makefile.

alteratively, override the following configuration variables by prepending it to the `docker compose` command.

- `POSTGRES_USER` default postgres user
- `POSTGRES_PASSWORD` password of default postgres user
- `POSTGRES_DB` db name
- `POSTGRES_PORT` postgres port to expose to host machine.


ie:
```
POSTGRES_USER=baby docker compose up --build -d
```


## Unit Testing ##

this project uses both [mockery](https://github.com/vektra/mockery) to generate mock interfaces 
and [sqlmock](https://github.com/DATA-DOG/go-sqlmock) to mock database expectations.

# Checklist #
list of things to complete

[x] docker compose

[x] dockerfile

[x]  documentation

[x] Postman collection

[ ] gitlab.ci (optional)

# API Endpoints #

this is a list of endpoints provided by the service, plus brief description of what to expect from each endpoint. For simplicity, a postman collection is also provided in `docs/postman`

## Register Student ##
Register students to teacher.

`POST /api/register`

- Returns `202 Accepted`, plus list of registered/unregistered students, in the event that an invalid user.

- Returns `204 No Content` if all students get registered/have been registered

- Returns `404 Not Found` if teacher doesnt exist

## Get Common Students ##

`GET /api/commonstudents`

Returns a list of students shared by the provided list of teachers. No validation is done on teachers provided

## Suspend Student ##

`POST /api/suspend`
suspends given student. 

- Returns `404 Not Found` if student doesnt exist


## RetriveStudentsForNotification ##

`POST /api/retrievefornotifications`

retrieves students of a teacher for notification. 
conditions: 
1. student isnt suspended
and 
2. student is enrolled to teacher or tagged in the notification

this endpoint does not validate if teacher exists


# Notes #

## regarding `ON CONFLICT` in SQL statements ##

this project utilizes 
`ON CONFLICT ** something **` while performing data manipulation for a bit of fun and to lessen the steps needed in certain data manipulation.

while its a fun feature in postgres, one must be careful with this function, as having a trigger on the table this query is run on will result in the sql engine not throwing errors on conflicts, thus executing the associated triggers.

in general, i would avoid using `ON CONFLICT` in most production workloads unless under specific conditions.

## regarding `returning` in SQL statements ##

The `RETURNING` keyword in PostgreSQL gives an opportunity to return from the insert or update statement the values of any columns after the insert or update was run.