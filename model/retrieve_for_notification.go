package model

type RetrieveForNotificationRequest struct {
	TeacherEmail string `json:"teacher" validate:"required,email"`
	Notification string `json:"notification"`
}
type RetrieveForNotificationResponse struct {
	Recipients []string `json:"recipients"`
}
