package model

type RegisterStudentRequest struct {
	Teacher  string   `json:"teacher" validate:"required,email"`
	Students []string `json:"students" validate:"gt=0,dive,email"`
}

// RegisterStudentResponse for
type RegisterStudentResponse struct {
	RegisteredStudent []string `json:"registered_student,omitempty"`
	InvalidStudent    []string `json:"invalid_student,omitempty"`
}
