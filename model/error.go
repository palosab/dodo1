package model

const (
	ErrBadRequest            = "BAD_REQUEST"
	ErrEntityNotFound        = "NOT_FOUND"
	ErrInternalServerError   = "INTERNAL_SERVER_ERROR"
	ErrInternalServerErrorDB = ErrInternalServerError + "_DB"
)

// Error response. implements error interface too ^^
type ErrorResponse struct {
	Code    string      `json:"code,omitempty"`
	Message string      `json:"message"`
	Status  int         `json:"-"`
	Data    interface{} `json:"data,omitempty"`
}

// NewError returns pointer to a new Error
func NewError(code string, message string, status int, data interface{}) *ErrorResponse {
	return &ErrorResponse{
		Code:    code,
		Message: message,
		Status:  status,
		Data:    data,
	}
}

// Error() implementation
func (e ErrorResponse) Error() string {
	return e.Message
}
