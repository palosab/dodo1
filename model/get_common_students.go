package model

type GetCommonStudentsResponse struct {
	Students []string `json:"students"`
}
