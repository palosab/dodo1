package pq

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Config struct {
	Host     string `json:"host"`
	Username string `json:"username"`
	Password string `json:"password"`
	Database string `json:"database"`
	SSLMode  string `json:"ssl_enabled"`
	Port     string `json:"port"`
}

type Options struct {
	dsn    *string `json:"-"`
	config *Config `json:"-"`
}

type ConnectionOptions func(*Options)

func WithDSN(dsn string) ConnectionOptions {
	return func(o *Options) {
		if "" != dsn {
			o.dsn = &dsn
		}
	}
}
func WithConfig(cfg *Config) ConnectionOptions {
	return func(o *Options) {
		o.config = cfg
	}
}

func (c *Config) DNS() string {
	if c.SSLMode == "" {
		c.SSLMode = "disable"
	}
	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=%s",
		c.Username,
		c.Password,
		c.Host,
		c.Port,
		c.Database,
		c.SSLMode,
	)

}

// NewDB initiates and returns a new db connection

func NewDB(opts ...ConnectionOptions) (*sqlx.DB, error) {
	var (
		options Options
		dsn     string = "postgres://postgres:pass@localhost:5432/db?sslmode=disable"
	)
	for _, opt := range opts {
		opt(&options)
	}
	if dnsPtr := options.dsn; dnsPtr != nil {
		dsn = *dnsPtr
	} else if options.config != nil {
		dsn = options.config.DNS()
	}
	db, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		fmt.Println(dsn)
		return nil, err
	}
	return db, nil
}

func MustNewDB(opts ...ConnectionOptions) *sqlx.DB {
	db, err := NewDB(opts...)
	if err != nil {
		panic(err)
	}
	return db
}
