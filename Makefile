
.EXPORT_ALL_VARIABLES:
POSTGRES_USER=postgres
POSTGRES_PASSWORD=pass
POSTGRES_DB=db
POSTGRES_PORT=5432

test: clean
	@go test ./...

testcover: clean
	@go test ./... -coverprofile cover.out 
	@go tool cover -func cover.out | grep total:
	@rm cover.out

clean:
	@go clean -testcache

generate-mocks:
	mockery --all --output=mocks/ --unroll-variadic=False

dockerdown: #tears down the containers
	docker compose down

dockerup: # starts up datastore, runs migration, app
	docker compose up --build -d 