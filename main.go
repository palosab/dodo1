package main

import (
	"assessment1/api"
	"assessment1/store"
	"assessment1/store/studentmgmt"
	"assessment1/utils/pq"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/cobra"
)

var (
	env        = os.Getenv("ENV")
	connection *sqlx.DB
)

func init() {

	connection = pq.MustNewDB(pq.WithConfig(&pq.Config{
		Host:     os.Getenv("DBHOST"),
		Username: os.Getenv("POSTGRES_USER"),
		Password: os.Getenv("POSTGRES_PASSWORD"),
		Database: os.Getenv("POSTGRES_DB"),
		Port:     "5432",
	}))
}
func main() {
	defer connection.Close()
	rootCmd := apiCmd()
	rootCmd.AddCommand(migrateCmd(), runCmd())
	rootCmd.Execute()

}

func runServer() {
	ds := studentmgmt.NewStore(connection)
	server := api.NewServer(ds)

	// fmt.Println("hello world")
	router := mux.NewRouter()
	apiSubRouter := router.PathPrefix("/api").Subrouter()
	server.SetupRoute(apiSubRouter)

	srv := &http.Server{
		Handler: router,
		Addr:    ":8000",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}

func migrateCmd() *cobra.Command {
	return &cobra.Command{
		Use: "migrate",
		Run: func(cmd *cobra.Command, args []string) {
			store.RunMigrate(connection)

		},
	}
}

func runCmd() *cobra.Command {
	return &cobra.Command{
		Use: "api",
		Run: func(cmd *cobra.Command, args []string) {
			runServer()
		},
	}
}
func apiCmd() *cobra.Command {
	return &cobra.Command{
		Short: "migrates db",
		Run: func(cmd *cobra.Command, args []string) {
			// fmt.Println("hello")
			store.RunMigrate(connection)
			runServer()
		},
	}
}
